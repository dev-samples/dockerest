package ro.hume.cosmin.sample.dockerest.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EchoController {

	@GetMapping("/echo")
	@ResponseStatus(HttpStatus.OK)
	public String echo(@RequestParam("text") String text) {
		return text;
	}
}
