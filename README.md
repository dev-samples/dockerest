# dockerest

An example [RESTful](http://en.wikipedia.org/wiki/Representational_state_transfer) web service in Java using [Spring Boot (RestController)](https://spring.io/guides/gs/rest-service/).

The service is packaged in a [Docker](https://www.docker.com/) image.

Acceptance tests implemented as a separate project, and are executed against the Docker image.

There's an [article with explanations](https://cosmin.hume.ro/2018/12/using-maven-and-docker-to-test-a-spring-rest-service/).
