package ro.hume.cosmin.sample.dockerest.api;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

import org.junit.BeforeClass;
import org.junit.Test;

public class EchoIT {

	private static int webPort;

	@BeforeClass
	public static void setUpClass() {
		webPort = Integer.parseInt(System.getProperty("webPort"));
	}

	@Test
	public void testEcho() {
		//@formatter:off
		given()
			.port(webPort)
		.when()
			.get("/echo?text=test")
		.then()
			.statusCode(200)
			.and()
			.body(is("test"));
		//@formatter:off
	}
}
